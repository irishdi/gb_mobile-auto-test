import io.appium.java_client.MobileDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class LoginTest {

    public static final String EXPECTED = "Please enter a valid email address";
    public static final String PASSWORD = "Qwerty";
    public static final String SAMPLETEXT = "trtrtrt";

    @Test
    public void checkEmptyEmail() throws MalformedURLException, InterruptedException {
        //Установка capabilities
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel_5_Android_11_");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("app","C:\\Users\\irish\\Downloads\\Android-NativeDemoApp-0.2.1.apk");

        //Открываем приложение
        MobileDriver driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);

        Thread.sleep(2000);
        //Нажимаем логин в основном меню
        MobileElement menuLoginButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Login\"]/android.view.ViewGroup/android.widget.TextView");
        menuLoginButton.click();
        Thread.sleep(2000);
        //Вводим пароль
        MobileElement inputPassword = (MobileElement) driver.findElementByAccessibilityId("input-password");
        inputPassword.click();
        inputPassword.sendKeys(PASSWORD);
        //Нажимаем логин
        MobileElement loginButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"button-LOGIN\"]/android.view.ViewGroup");
        loginButton.click();
        Thread.sleep(2000);
        //Проверяем текст ошибки в поле email
        MobileElement emailError = (MobileElement) driver.findElementByXPath("//android.widget.ScrollView[@content-desc=\"Login-screen\"]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[4]/android.widget.TextView[1]");
        Assert.assertEquals(emailError.getText(), EXPECTED);

        driver.quit();

    }

    @Test
    public void checkFormField() throws MalformedURLException, InterruptedException {
        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability("deviceName", "Pixel_5_Android_11_");
        capabilities.setCapability("udid", "emulator-5554");
        capabilities.setCapability("platformName", "Android");
        capabilities.setCapability("platformVersion", "11");
        capabilities.setCapability("app","C:\\Users\\irish\\Downloads\\Android-NativeDemoApp-0.2.1.apk");

        MobileDriver driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), capabilities);

        Thread.sleep(2000);
        MobileElement menuFormsButton = (MobileElement) driver.findElementByXPath("//android.view.ViewGroup[@content-desc=\"Forms\"]/android.view.ViewGroup/android.widget.TextView");
        menuFormsButton.click();
        Thread.sleep(2000);
        MobileElement inputField = (MobileElement) driver.findElementByAccessibilityId("text-input");
        inputField.click();
        inputField.sendKeys(SAMPLETEXT);
        Thread.sleep(2000);
        MobileElement inputTextResult= (MobileElement) driver.findElementByAccessibilityId("input-text-result");
        Assert.assertEquals(inputTextResult.getText(), SAMPLETEXT);
        driver.quit();
    }

}
